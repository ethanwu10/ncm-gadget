#!/bin/bash

if [ "${EUID}" -ne 0 ]; then
	echo 'ERROR: must be run as root' >&2
	exit -1
fi

set -x

cp -i ${PWD}/ncm-gadget /usr/local/bin/ncm-gadget
cp -i ${PWD}/ncm-gadget.service /etc/systemd/system/ncm-gadget.service
cp -i ${PWD}/usb0.interface /etc/network/interfaces.d/usb0

set +x
cat <<EOF

- Systemd unit ncm-gadget.service installed. To enable it, run
   # systemctl enable ncm-gadget.service
- Network interface config installed at /etc/network/interfaces.d/usb0
  with default static address 10.12.34.2/24

EOF
set -x
